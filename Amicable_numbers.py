print('Made by Sergei Vdovin')
print('Amicable numbers:')

def factorialSum(num):
    sum = 0
    for i in range (1, num):
        if num % i == 0:
            sum += i
    return sum

for num1 in range (1, 10000 + 1):
    num2 = factorialSum(num1)
    if num1 < num2 < 10000 and factorialSum(num2) == num1:
        print(num1,' ', num2)
